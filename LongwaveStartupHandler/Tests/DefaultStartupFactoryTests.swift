//
//  DefaultStartupFactoryTests.swift
//  StartupHandlerTests
//
//  Created by Alessio Bonu on 14/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import XCTest
@testable import StartupHandler

class DefaultStartupFactoryTests: XCTestCase {

    func testLoadingViewControllerIsReturnedSuccessfully() {
        let loadingViewController = LoadingFlowDisplayableViewcontrollerMock()
        let sut = DefaultFlowFactory(loadingViewController: loadingViewController, mainFlowViewController: MainFlowViewControllerMock())
        XCTAssertEqual(loadingViewController, sut.loadingFlow())
    }
    
    func testDefaultPopupIsAUIAlertController() {
        let loadingViewController = LoadingFlowDisplayableViewcontrollerMock()
        let sut = DefaultFlowFactory(loadingViewController: loadingViewController, mainFlowViewController: MainFlowViewControllerMock())
        XCTAssertTrue(sut.startupErrorViewController(onReload: nil) is UIAlertController)
    }
    
    func testMainFlowIsReturned() {
        let mainFlow = MainFlowViewControllerMock()
        let loadingViewController = LoadingFlowDisplayableViewcontrollerMock()
        let sut = DefaultFlowFactory(loadingViewController: loadingViewController, mainFlowViewController: mainFlow)
        XCTAssertTrue(sut.mainFlow() is MainFlowViewControllerMock)
    }
}
