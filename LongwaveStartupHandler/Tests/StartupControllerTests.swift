//
//  StartupControllerTests.swift
//  StartupHandlerTests
//
//  Created by Alessio Bonu on 13/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import XCTest
@testable import StartupHandler
import LongwaveUtils
import LongwaveAuthenticationHandler
import LongwaveNetworkUtils

class StartupControllerTests: XCTestCase {
    
    private func startupController(flowFactory: StartupFlowsFactory,
                                   startupService: StartupServiceMock = StartupServiceMock(),
                                   startupConfgurator: StartupConfigurator  = StartupConfigurator(),
                                   authenticationHandler: AuthenticationFlow? = nil) -> StartupController {
        let dependency = StartupDependencyContainer(flowFactory: flowFactory,
                                                    startupService: startupService,
                                                    startupConfigurator: startupConfgurator,
                                                    authenticationHandler: authenticationHandler)
        let controller = StartupController(dependencyContainer: dependency)
        return controller
    }
    
    private func getRootViewController() -> NavigationMock {
        return NavigationMock()
    }
    
    func testStartupControllerShouldLoadLoadingControllerOnStart() {
        let flowFactory = DefaultStartupFlowsFactoryMock()
        let sut = startupController(flowFactory: flowFactory)
        var loadingCalled = false
        flowFactory.onLoadingFlowRequested = { loadingVC in
            loadingCalled = true
        }
        sut.start(responceType: LoginResponseMock.self, on: getRootViewController())
        XCTAssertTrue(loadingCalled)
    }
    
    func testStartupControllerShouldPresentLoadingFlowOnRootViewController() {
        let flowFactory = DefaultStartupFlowsFactoryMock()
        let viewControllerToPresent = LoadingFlowDisplayableViewcontrollerMock()
        flowFactory.loadingViewController = viewControllerToPresent
        let sut = startupController(flowFactory: flowFactory)
        let rootViewController = getRootViewController()
        var presentCalled = false
        rootViewController.onEmbedCalled = { viewControllerToPresent in
            presentCalled = true
            XCTAssertEqual(viewControllerToPresent, viewControllerToPresent)
        }
        sut.start(responceType: LoginResponseMock.self, on: rootViewController)
        XCTAssertTrue(presentCalled)
    }
    
    func testIfNotSetDefaultLoaderIsShown() {
        class FinderMock: StoryboardFinder {
            var storyboardName: String = "Loading"
             
            func storyboardId(for viewControllerId: String) throws -> String {
                return storyboardName
            }
            func viewControllers(inStoryboard id: String) -> [String] {
                return []
            }
            func storyboard(for id: String, bundle: Bundle?) -> StoryboardRetriever {
                return UIStoryboard(name: id, bundle: Bundle(for: StartupController.self))
            }
        }
        let flowFactory = DefaultFlowFactory(loadingViewController: nil,
                                             mainFlowViewController: MainFlowViewControllerMock(),
                                             finder: FinderMock())
        let sut = startupController(flowFactory: flowFactory)
        let rootViewController = getRootViewController()
        var onPresentDidCall = false
        var count = 0
        rootViewController.onEmbedCalled = { vc in
            if count == 0 {
                XCTAssertTrue(vc is DefaultLoadingFlowDisplayableViewController)
                _ = vc.view
            }
            count += 1
            onPresentDidCall = true
        }
        sut.start(responceType: LoginResponseMock.self, on: rootViewController)
        XCTAssertTrue(onPresentDidCall)
    }
    
    func testAfterShowingLoadingControllerServiceIsCalled() {
        let flowFactory = DefaultStartupFlowsFactoryMock()
        let serviceMock = StartupServiceMock()
        var onLoadConfigurationDidCall = false
        serviceMock.onLoadDidCall = {
            onLoadConfigurationDidCall = true
            return (result: nil, error: nil)
        }
        let sut = startupController(flowFactory: flowFactory,
                                    startupService: serviceMock)
        sut.start(responceType: LoginResponseMock.self, on: getRootViewController())
        XCTAssertTrue(onLoadConfigurationDidCall)
    }
    
    func testAfterServiceIsCalledWithErrorAPopupIsPresented() {
        let flowFactory = DefaultStartupFlowsFactoryMock()
        let loadingMock = LoadingFlowDisplayableViewcontrollerMock()
        flowFactory.loadingViewController = loadingMock
        let serviceMock = StartupServiceMock()
        var poupPresentationDidCall = false
        serviceMock.onLoadDidCall = {
            return (result: nil, error: StartupTestError.testError)
        }
        let sut = startupController(flowFactory: flowFactory,
                                    startupService: serviceMock)
        let presenter = getRootViewController()
        loadingMock.onPresentCalled = { vc in
            poupPresentationDidCall = true
            XCTAssertTrue(vc is UIAlertController)
        }
        sut.start(responceType: LoginResponseMock.self, on: presenter)
        XCTAssertTrue(poupPresentationDidCall)
    }
    
    func testServiceIsCalledWithErrorWhenTheUserTapOnReloadTheServiceIsCalledAgain() {
        let flowFactory = DefaultStartupFlowsFactoryMock()
        let loadingMock = LoadingFlowDisplayableViewcontrollerMock()
        flowFactory.loadingViewController = loadingMock
        let serviceMock = StartupServiceMock()
        var poupPresentationDidCall = false
        var serviceCallCount = 0
        serviceMock.onLoadDidCall = {
            serviceCallCount += 1
            return (result: nil, error: StartupTestError.testError)
        }
        let sut = startupController(flowFactory: flowFactory,
                                    startupService: serviceMock)
        let presenter = getRootViewController()
        loadingMock.onPresentCalled = { vc in
            poupPresentationDidCall = true
            if serviceCallCount == 1 {
                flowFactory.fireReload()
            }
        }
        sut.start(responceType: LoginResponseMock.self, on: presenter)
        XCTAssertTrue(poupPresentationDidCall)
        XCTAssertEqual(serviceCallCount, 2)
    }
    
    private func loadConfigurationAndMainFlow(isReady: Bool,
                                              onDidLoad: @escaping () -> Void) {
        let flowFactory = DefaultStartupFlowsFactoryMock()
        let loadingMock = LoadingFlowDisplayableViewcontrollerMock()
        flowFactory.loadingViewController = loadingMock
        let serviceMock = StartupServiceMock()
        serviceMock.onLoadDidCall = {
            return (result: ["baseUrl": URL(string: "www.google.com")!], error: nil)
        }
        let sut = startupController(flowFactory: flowFactory,
                                    startupService: serviceMock)
        sut.onApiRequestProviderNeeded = {
            return HTTPRequestProviderMock()
        }
        sut.onApiEngineReady = { _, _, ready in
            ready(isReady)
        }
        sut.onMainFlowRequested = {
            onDidLoad()
            return StartingFlowMock()
        }
        let presenter = getRootViewController()
        sut.start(responceType: LoginResponseMock.self, on: presenter)
    }
    
    func testAfterServiceIsCalledWithSuccessShouldAskForTheMainFlowToTheCaller() {
        var didRequestMainFlow = false
        loadConfigurationAndMainFlow(isReady: true) {
            didRequestMainFlow = true
        }
        XCTAssertTrue(didRequestMainFlow)
    }
    
    func testAfterServiceIsCalledWithSuccessShouldNotAskForTheMainFlowToTheCaller() {
        var didRequestMainFlow = false
        loadConfigurationAndMainFlow(isReady: false) {
            didRequestMainFlow = true
        }
        XCTAssertFalse(didRequestMainFlow)
    }
    
    func testAfterServiceIsCalledWithSuccessShouldPresentTheMainFlow() {
        let flowFactory = DefaultStartupFlowsFactoryMock()
        let loadingMock = LoadingFlowDisplayableViewcontrollerMock()
        flowFactory.loadingViewController = loadingMock
        let serviceMock = StartupServiceMock()
        var presentationDidCall = false
        serviceMock.onLoadDidCall = {
            return (result: ["baseUrl": URL(string: "www.google.com")!], error: nil)
        }
        let sut = startupController(flowFactory: flowFactory,
                                    startupService: serviceMock)
        sut.onApiRequestProviderNeeded = {
            return HTTPRequestProviderMock()
        }
        sut.onApiEngineReady = { _, _, ready in
            ready(true)
        }
        let startingFlow = StartingFlowMock()
        startingFlow.onPresent = {
            presentationDidCall = true
        }
        sut.onMainFlowRequested = {
            return startingFlow
        }
        let presenter = getRootViewController()
        sut.start(responceType: LoginResponseMock.self, on: presenter)
        XCTAssertTrue(presentationDidCall)
    }
    
    func testWhenConfigurationServiceIsCalledSuccessfullyServiceApiEngineIsCreatedAndProvidedToTheCaller() {
        let flowFactory = DefaultStartupFlowsFactoryMock()
        let loadingMock = LoadingFlowDisplayableViewcontrollerMock()
        flowFactory.loadingViewController = loadingMock
        let serviceMock = StartupServiceMock()
        var didCallApiEngineReady = false
        serviceMock.onLoadDidCall = {
            return (result: ["baseUrl": URL(string: "www.google.com")!],
                    error: nil)
        }
        let sut = startupController(flowFactory: flowFactory,
                                    startupService: serviceMock)
        let presenter = getRootViewController()
        sut.onApiRequestProviderNeeded = {
            return HTTPRequestProviderMock()
        }
        sut.onApiEngineReady = { _, _, _ in
            didCallApiEngineReady = true
        }
        sut.start(responceType: LoginResponseMock.self, on: presenter)
        XCTAssertTrue(didCallApiEngineReady)
    }
    
    func testWhenConfigurationServiceIsCalledSuccessfullyServiceApiEngineIsNotCreatedIfNEtworkHandlerIsNotProvided() {
        let flowFactory = DefaultStartupFlowsFactoryMock()
        let loadingMock = LoadingFlowDisplayableViewcontrollerMock()
        flowFactory.loadingViewController = loadingMock
        let serviceMock = StartupServiceMock()
        var didCallApiEngineReady = false
        serviceMock.onLoadDidCall = {
            return (result: nil, error: nil)
        }
        let sut = startupController(flowFactory: flowFactory,
                                    startupService: serviceMock)
        let presenter = getRootViewController()
        sut.onApiEngineReady = { _, _, _ in
            didCallApiEngineReady = true
        }
        sut.start(responceType: LoginResponseMock.self, on: presenter)
        XCTAssertFalse(didCallApiEngineReady)
    }
    
    func testWhenConfigurationServiceIsCalledAndAPiEngineCreatedShouldSetupAuthenticationFlow() {
        let flowFactory = DefaultStartupFlowsFactoryMock()
        let loadingMock = LoadingFlowDisplayableViewcontrollerMock()
        flowFactory.loadingViewController = loadingMock
        let serviceMock = StartupServiceMock()
        var didCallSetup = false
        serviceMock.onLoadDidCall = {
            return (result: ["baseUrl": URL(string: "www.google.com")!],
                    error: nil)
        }
        let authFlowMock = AuthenticationFlowMock()
        authFlowMock.onSetup = {
            didCallSetup = true
        }
        let sut = startupController(flowFactory: flowFactory,
                                    startupService: serviceMock,
                                    authenticationHandler: authFlowMock)
        sut.onSecureSerializer = {
            return SecureSerializerMock()
        }
        let presenter = getRootViewController()
        sut.onApiRequestProviderNeeded = {
            return HTTPRequestProviderMock()
        }
        sut.start(responceType: LoginResponseMock.self, on: presenter)
        XCTAssertTrue(didCallSetup)
    }
    
    
    func testAfterServiceIsCalledWithSuccessTheMainFlowIsEmbedded() {
        let flowFactory = DefaultStartupFlowsFactoryMock()
        let loadingMock = LoadingFlowDisplayableViewcontrollerMock()
        flowFactory.loadingViewController = loadingMock
        let serviceMock = StartupServiceMock()
        var presentationDidCall = false
        serviceMock.onLoadDidCall = {
            return (result: nil, error: nil)
        }
        let sut = startupController(flowFactory: flowFactory,
                                    startupService: serviceMock)
        let presenter = getRootViewController()
        
        presenter.onEmbedCalled = { vc in
            presentationDidCall = true
            XCTAssertFalse(vc is UIAlertController)
        }
        sut.start(responceType: LoginResponseMock.self, on: presenter)
        XCTAssertTrue(presentationDidCall)
    }
    
    func testAfterServiceIsCalledWithSuccessTheLoadingFlowIsDisembedded() {
        let flowFactory = DefaultStartupFlowsFactoryMock()
        let loadingMock = LoadingFlowDisplayableViewcontrollerMock()
        flowFactory.loadingViewController = loadingMock
        let serviceMock = StartupServiceMock()
        var disembedDidCall = false
        serviceMock.onLoadDidCall = {
            return (result: ["baseUrl": URL(string: "www.google.com")!], error: nil)
        }
        let sut = startupController(flowFactory: flowFactory,
                                    startupService: serviceMock)
        sut.onApiRequestProviderNeeded = {
            return HTTPRequestProviderMock()
        }
        sut.onApiEngineReady = { _, _, ready in
            ready(true)
        }
        let presenter = getRootViewController()
        loadingMock.onDisembedCalled = { vc in
            disembedDidCall = true
            XCTAssertFalse(vc is UIAlertController)
        }
        sut.start(responceType: LoginResponseMock.self, on: presenter)
        XCTAssertTrue(disembedDidCall)
    }
    
    func testIfLoginIsMandatoryAuthenticationFlowIsPresented() {
        let flowFactory = DefaultStartupFlowsFactoryMock()
        let loadingMock = LoadingFlowDisplayableViewcontrollerMock()
        flowFactory.loadingViewController = loadingMock
        let serviceMock = StartupServiceMock()
        var authenticationDidRequest = false
        serviceMock.onLoadDidCall = {
            return (result: ["baseUrl": URL(string: "www.google.com")!], error: nil)
        }
        let authenticationMock = AuthenticationFlowMock()
        let startupConfigurator = StartupConfigurator(authenticationMandatory: true)
        let sut = startupController(flowFactory: flowFactory,
                                    startupService: serviceMock,
                                    startupConfgurator: startupConfigurator,
                                    authenticationHandler: authenticationMock)
        sut.onApiRequestProviderNeeded = {
            return HTTPRequestProviderMock()
        }
        sut.onApiEngineReady = { _, _, ready in
            ready(true)
        }
        
        let presenter = getRootViewController()
        authenticationMock.onAuthenticateDidRequest = { presenter, completion in
            authenticationDidRequest = true
        }
        sut.start(responceType: LoginResponseMock.self, on: presenter)
        XCTAssertTrue(authenticationDidRequest)
    }
    
    func testAuthenticationFlowCompletedSuccessfullyAndMainFlowIsLoaded() {
        let flowFactory = DefaultStartupFlowsFactoryMock()
        let mainFlowMock = MainFlowViewControllerMock()
        flowFactory.mainFlowMock = mainFlowMock
        let serviceMock = StartupServiceMock()
        var mainFlowEmbedded = false
        serviceMock.onLoadDidCall = {
            return (result: ["baseUrl": URL(string: "www.google.com")!], error: nil)
        }
        let authenticationMock = AuthenticationFlowMock()
        let startupConfigurator = StartupConfigurator(authenticationMandatory: true)
        let sut = startupController(flowFactory: flowFactory,
                                    startupService: serviceMock,
                                    startupConfgurator: startupConfigurator,
                                    authenticationHandler: authenticationMock)
        sut.onApiRequestProviderNeeded = {
            return HTTPRequestProviderMock()
        }
        sut.onApiEngineReady = { _, _, ready in
            ready(true)
        }
        
        let presenter = getRootViewController()
        authenticationMock.onAuthenticateDidRequest = { presenter, completion in
            completion(.success(LoginResponseMock()))
        }
        presenter.onEmbedCalled = { vc in
            if vc == mainFlowMock {
                mainFlowEmbedded = true
            }
        }        
        sut.start(responceType: LoginResponseMock.self, on: presenter)
        XCTAssertTrue(mainFlowEmbedded)
    }
    
    func testAuthenticationFlowCompletedWithErrorAndErrorIsShown() {
        let flowFactory = DefaultStartupFlowsFactoryMock()
        let loadingMock = LoadingFlowDisplayableViewcontrollerMock()
        flowFactory.loadingViewController = loadingMock
        let serviceMock = StartupServiceMock()
        var errorPopupDisplayed = false
        serviceMock.onLoadDidCall = {
            return (result: ["baseUrl": URL(string: "www.google.com")!], error: nil)
        }
        let authenticationMock = AuthenticationFlowMock()
        let startupConfigurator = StartupConfigurator(authenticationMandatory: true)
        let sut = startupController(flowFactory: flowFactory,
                                    startupService: serviceMock,
                                    startupConfgurator: startupConfigurator,
                                    authenticationHandler: authenticationMock)
        sut.onApiRequestProviderNeeded = {
            return HTTPRequestProviderMock()
        }
        sut.onApiEngineReady = { _, _, ready in
            ready(true)
        }
        
        let presenter = getRootViewController()
        authenticationMock.onAuthenticateDidRequest = { presenter, completion in
            completion(.failure(StartupTestError.testError))
        }
        loadingMock.onPresentCalled = { vc in
            if vc is UIAlertController {
                errorPopupDisplayed = true
            }
        }
        sut.start(responceType: LoginResponseMock.self, on: presenter)
        XCTAssertTrue(errorPopupDisplayed)
    }
}

/// MARK - TokenSerialization
extension StartupControllerTests {
    
    func testActivityIndicatorAnimationStartsWhenLoadingConfiguration() {
        let flowFactory = DefaultStartupFlowsFactoryMock()
        let loadingMock = LoadingFlowDisplayableViewcontrollerMock()
        flowFactory.loadingViewController = loadingMock
        let serviceMock = StartupServiceMock()
        var poupPresentationDidCall = false
        var activityAnimationDidStart = false
        loadingMock.onStartLoading = {
            activityAnimationDidStart = true
        }
        serviceMock.onLoadDidCall = {
            return (result: nil, error: StartupTestError.testError)
        }
        let sut = startupController(flowFactory: flowFactory,
                                    startupService: serviceMock)
        let presenter = getRootViewController()
        loadingMock.onPresentCalled = { vc in
            poupPresentationDidCall = true
            XCTAssertTrue(vc is UIAlertController)
        }
        sut.start(responceType: LoginResponseMock.self, on: presenter)
        XCTAssertTrue(poupPresentationDidCall)
        XCTAssertTrue(activityAnimationDidStart)        
    }
}

/// MARK - Push notification
extension StartupControllerTests {
    
}
