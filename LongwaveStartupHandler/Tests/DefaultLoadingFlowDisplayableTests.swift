//
//  DefaultLoadingFlowDisplayableTests.swift
//  StartupHandlerTests
//
//  Created by Alessio Bonu on 21/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import XCTest
import LongwaveUtils
@testable import StartupHandler

class DefaultLoadingFlowDisplayableTests: XCTestCase {
    
    func testLoadingViewControllerShouldSetupOnLoad() {
        let storyboardFinder = StoryboardFinderMock()
        do {
            let id = "DefaultLoadingFlowDisplayableViewController"
            let sut: DefaultLoadingFlowDisplayableViewController =
                try DefaultStoryboardLoader.viewController(with: id,
                                                           finder: storyboardFinder)
            let _ = sut.view
            XCTAssertTrue(sut.activityIndicator.isHidden)
            XCTAssertFalse(sut.activityIndicator.isAnimating)
        } catch {
            XCTFail("Failed creating sut")
        }
        
    }
    
    func testLoadingViewControllerStartAnimationSuccessfully() {
        class ActivityMock: UIActivityIndicatorView {
            var onStartAnimating: (() -> Void)?
            override func startAnimating() {
                onStartAnimating?()
            }
        }
        let sut = DefaultLoadingFlowDisplayableViewController()
        let activity = ActivityMock()
        var startCalled = false
        activity.onStartAnimating = {
            startCalled = true
        }
        sut.activityIndicator = activity
        sut.startLoading()
        XCTAssertTrue(startCalled)
    }
    
    func testLoadingViewControllerStopAnimationSuccessfully() {
        class ActivityMock: UIActivityIndicatorView {
            var onEndAnimating: (() -> Void)?
            override func stopAnimating() {
                onEndAnimating?()
            }
        }
        let sut = DefaultLoadingFlowDisplayableViewController()
        let activity = ActivityMock()
        var stopCalled = false
        activity.onEndAnimating = {
            stopCalled = true
        }
        sut.activityIndicator = activity
        sut.endLoading()
        XCTAssertTrue(stopCalled)
    }
}
