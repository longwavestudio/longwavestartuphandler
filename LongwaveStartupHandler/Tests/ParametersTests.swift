//
//  ParametersTests.swift
//  StartupHandlerTests
//
//  Created by Alessio Bonu on 03/12/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import XCTest
@testable import StartupHandler
class ParametersTests: XCTestCase {

    func testParametersLookupWithOneParameterPresentShouldReturnValue() {
        enum TestParameter: String {
            case identifier
        }
        let expectedValue = "aaa"
        let parameters = Parameters(map: [TestParameter.identifier: expectedValue])
        XCTAssertEqual(parameters.lookup(TestParameter.identifier), expectedValue)
    }

    func testParametersLookupWithNoParameterPresentShouldReturnNil() {
        enum TestParameter: String {
            case identifier
        }
        let expectedValue: [TestParameter: String] = [:]
        let parameters = Parameters(map: expectedValue)
        XCTAssertNil(parameters.lookup(TestParameter.identifier))
    }
}
