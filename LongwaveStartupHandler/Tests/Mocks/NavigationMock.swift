//
//  NavigationMock.swift
//  StartupHandlerTests
//
//  Created by Alessio Bonu on 13/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit

class NavigationMock: UIViewController {
    var onPresentCalled: ((_ viewController: UIViewController) -> Void)?
    var onEmbedCalled: ((_ viewController: UIViewController) -> Void)?
    var onDisembedCalled: ((_ viewController: UIViewController) -> Void)?

    override func present(_ viewControllerToPresent: UIViewController, animated flag: Bool, completion: (() -> Void)? = nil) {
        onPresentCalled?(viewControllerToPresent)
    }
    
    override func embed(viewController: UIViewController, in viewToEmbed: UIView? = nil) -> UIViewController {
        onEmbedCalled?(viewController)
        return viewController
    }
    
    override func removeEmbeddedViewController(embeddedViewController: UIViewController) {
        onDisembedCalled?(embeddedViewController)
    }
}


