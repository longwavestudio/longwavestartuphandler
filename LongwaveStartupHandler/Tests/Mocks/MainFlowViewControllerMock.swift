//
//  MainFlowViewControllerMock.swift
//  StartupHandlerTests
//
//  Created by Alessio Bonu on 03/12/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
@testable import StartupHandler

final class MainFlowViewControllerMock: UIViewController {
    var onNavigate: ((_ path: String) -> Void)?
    var onResetNavigation: (() -> Void)?

}

extension MainFlowViewControllerMock: PathResolver {
    func navigate(to path: String) {
        onNavigate?(path)
    }
    
    func resetNavigation() {
        onResetNavigation?()
    }
}
