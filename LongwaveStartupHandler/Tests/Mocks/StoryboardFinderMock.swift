//
//  StoryboardFinderMock.swift
//  StartupHandlerTests
//
//  Created by Alessio Bonu on 21/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
@testable import LongwaveUtils
@testable import StartupHandler

final class StoryboardFinderMock: StoryboardFinder {
    var storyboardName: String = "Loading"
    
    func storyboardId(for viewControllerId: String) -> String {
        return storyboardName
    }
    
    func viewControllers(inStoryboard id: String) -> [String] {
        return []
    }
    
    func storyboard(for id: String, bundle: Bundle?) -> StoryboardRetriever {
        return UIStoryboard(name: id, bundle: Bundle(for: DefaultLoadingFlowDisplayableViewController.self))
    }
}
