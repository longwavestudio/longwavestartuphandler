//
//  HTTPRequestProviderMock.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 20/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation
import LongwaveNetworkUtils
import Combine
final class HTTPRequestProviderMock {
    
}

extension HTTPRequestProviderMock: HTTPRequestProvider {
    func request<ResponseType>(_ api: APIEndpoint, baseURL: URL, headers: [APIHttpHeader]?, endpointParameters: [String : String], parameters: [String : Any]?) -> Future<ResponseType, Error> where ResponseType : Decodable {
        return Future<ResponseType, Error>() { _ in }
    }
    
    func retrieveConfiguration(completion: @escaping OnConfigurationDidRetrieve) {
        
    }
    
    func request<ResponseType>(_ api: APIEndpoint, baseURL: URL, headers: [APIHttpHeader]?, endpointParameters: [String : String], parameters: [String : Any]?, completion: @escaping OnRequestDidComplete<ResponseType>) where ResponseType : Decodable {
        
    }     
}
