//
//  StartingFlowMock.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 19/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit
@testable import StartupHandler

final class StartingFlowMock {
    var onPresent: (() -> Void)?
}

extension StartingFlowMock: StartingFlow {
    func present(on presenter: UIViewController, onCompletion: @escaping () -> Void) {
        onPresent?()
    }    
}
