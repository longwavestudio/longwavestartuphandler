//
//  LoadingFlowDisplayableViewcontrollerMock.swift
//  StartupHandlerTests
//
//  Created by Alessio Bonu on 13/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
@testable import StartupHandler

final class LoadingFlowDisplayableViewcontrollerMock: NavigationMock {
    var onStartLoading: (() -> Void)?
    var onEndLoading: (() -> Void)?

}

extension LoadingFlowDisplayableViewcontrollerMock: LoadingViewControllerAnimatable {
    func startLoading() {
        onStartLoading?()
    }
    
    func endLoading() {
        onEndLoading?()
    }
    
    
}
