//
//  LoginResponseMock.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 20/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation
import LongwaveNetworkUtils

struct LoginResponseMock {
    
}

extension LoginResponseMock: LoginResponse {
    func tokens() -> (accessToken: String, refreshToken: String)? {
        return (accessToken: "", refreshToken: "")
    }
}
