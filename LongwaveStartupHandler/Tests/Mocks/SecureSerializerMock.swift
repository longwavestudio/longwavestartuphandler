//
//  SecureSerializerMock.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 20/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation
import  LongwaveNetworkUtils

final class SecureSerializerMock {
    
}

extension SecureSerializerMock: SecureSerializer {
    func store(value: String, forKey: String) {
        
    }
    
    func load(_ key: String) -> String? {
        return ""
    }
    
    func delete(_ key: String) {
        
    }
}
