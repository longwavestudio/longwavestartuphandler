//
//  AuthenticationFlowMock.swift
//  StartupHandlerTests
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
@testable import StartupHandler
@testable import LongwaveAuthenticationHandler
import LongwaveNetworkUtils

enum AuthenticationTestError: Error {
    case authenticationError
    case refreshError
    case genericError
}

final class AuthenticationFlowMock {
    var onCustomHeaderRequested: OnCustomHeaderRequested?
    var onThemeRequested: OnThemeRequested = {
        return AuthTheme.defaultTheme()
    }
    var onAuthenticateDidRequest: ((_ presenter: UIViewController, _ completion: (Result<LoginResponseMock?, Error>) -> Void) -> Void)?
    var onRefreshDidRequest: ((_ completion: (Result<LoginResponseMock, Error>) -> Void) -> Void)?
    
    var onSetup: (() -> Void)?
}

extension AuthenticationFlowMock: AuthenticationFlow {
    func setup(service: AuthenticationProvider) {
        onSetup?()
    }

    
    func authenticate<T: LoginResponse>(on presenter: UIViewController, completion: @escaping (Result<T?, Error>) -> Void) {
        onAuthenticateDidRequest?(presenter, completion as! (Result<LoginResponseMock?, Error>) -> Void)
    }
    
    func refresh<T: LoginResponse>(completion: @escaping (Result<T, Error>) -> Void) {
        onRefreshDidRequest?(completion as! (Result<LoginResponseMock, Error>) -> Void)
    }
    
    
}
