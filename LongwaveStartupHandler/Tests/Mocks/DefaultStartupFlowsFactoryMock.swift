//
//  DefaultStartupFlowsFactoryMock.swift
//  StartupHandlerTests
//
//  Created by Alessio Bonu on 13/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
@testable import StartupHandler

final class DefaultStartupFlowsFactoryMock {
    var loadingViewController: LoadingFlowDisplayable?
    var mainFlowMock: MainFlowViewController?
    var onLoadingFlowRequested: ((LoadingFlowDisplayable) -> Void)?
    var onReload: OnReloadBlock?
    
    func fireReload() {
        onReload?()
    }
}

extension DefaultStartupFlowsFactoryMock: StartupFlowsFactory {
    
    func loadingFlow() -> LoadingFlowDisplayable {
        let loadingVC = loadingViewController ?? LoadingFlowDisplayableViewcontrollerMock()
        onLoadingFlowRequested?(loadingVC)
        return loadingVC
    }
    
    func startupErrorViewController(onReload: OnReloadBlock?) -> UIViewController {
        self.onReload = onReload
        let alert = UIAlertController(title: "Configuration error", message: "Error downloading configuration file.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Reload".localizedString(), style: .default, handler: { _ in
            onReload?()
        }))
        return alert
    }
    
    func mainFlow() -> MainFlowViewController? {
        return mainFlowMock ?? MainFlowViewControllerMock()
    }
}
