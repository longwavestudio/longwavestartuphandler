//
//  StartupServiceMock.swift
//  StartupHandlerTests
//
//  Created by Alessio Bonu on 21/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation
@testable import StartupHandler
enum StartupTestError: Error {
    case testError
}

final class StartupServiceMock {
    var error: Error? = nil
    var onLoadDidCall: (() -> (result: [AnyHashable: Any]?, error: Error?))?
    
    func setTestError() {
        error = StartupTestError.testError
    }
}

extension StartupServiceMock: StartupService {
    func loadConfiguration(completion: (([AnyHashable: Any]?, Error?) -> Void)?) {
        let toRet = onLoadDidCall?()
        completion?(toRet?.result, toRet?.error)
    }
}
