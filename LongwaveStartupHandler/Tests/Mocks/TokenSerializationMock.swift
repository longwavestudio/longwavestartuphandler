//
//  TokenSerializationMock.swift
//  StartupHandlerTests
//
//  Created by Alessio Bonu on 03/12/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation

@testable import StartupHandler
@testable import LongwaveAuthenticationHandler

final class TokenSerializationMock {
    var onTokenSerializationRequired: ((Token) -> Void)?
}

extension TokenSerializationMock: TokenSerialization {
    func serializeToken(_ token: Token) {
        onTokenSerializationRequired?(token)
    }
}
