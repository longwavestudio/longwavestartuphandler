//
//  LoadingStoryboardFinderTests.swift
//  StartupHandlerTests
//
//  Created by Alessio Bonu on 21/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import XCTest
@testable import StartupHandler

class LoadingStoryboardFinderTests: XCTestCase {
  
    func testViewControllersShouldReturnOneElement() {
        let sut = LoadingStoryboardFinder()
        XCTAssertEqual(sut.viewControllers(inStoryboard: "Loading").count, 1)
    }
    
    func testViewControllersShouldReturnDefaultLoadingFlowDisplayableViewController() {
        let sut = LoadingStoryboardFinder()
        XCTAssertEqual(sut.viewControllers(inStoryboard: "Loading").first, "DefaultLoadingFlowDisplayableViewController")
    }
    
    func testStoryboardIdIsLoadingForDefaultLoadingFlowDisplayableViewController() {
        let sut = LoadingStoryboardFinder()
        XCTAssertEqual(try sut.storyboardId(for: "DefaultLoadingFlowDisplayableViewController"), "Loading")
    }
    
    func testStoryboardIdThrowsForUnknonwnId() {
        let sut = LoadingStoryboardFinder()
        XCTAssertThrowsError(try sut.storyboardId(for: "yyyy"))
    }
}
