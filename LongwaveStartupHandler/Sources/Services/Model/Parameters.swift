//
//  Parameters.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 03/12/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation

/// Parameters carried from the Universal link URL
public struct Parameters {
    private var map: [String: String] = [:]
    
    public init<DeepLinkParameter>(map: [DeepLinkParameter: String]) where DeepLinkParameter: RawRepresentable, DeepLinkParameter.RawValue == String {
        for (k, v) in map {
            self.map[k.rawValue] = v
        }
    }
    /// Retrieves the parameter parsed from the URL returned by the UiniversalLink callback
    /// - Parameter parameter: the parameter to be
    public func lookup<DeepLinkParameter>(_ parameter: DeepLinkParameter) -> String? where DeepLinkParameter: RawRepresentable, DeepLinkParameter.RawValue == String {
        return map[parameter.rawValue]
    }
}


