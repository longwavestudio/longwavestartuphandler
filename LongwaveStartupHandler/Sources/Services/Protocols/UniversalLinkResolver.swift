//
//  UniversalLinkResolver.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 03/12/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation

protocol UniversalLinkResolver {
    /// Called to parse the URL and retrieve all parameters from it.
    /// Generic on DeepLinkParameter. You can use a String enum to define all the parameters needed
    /// - Parameter path: the path received by the system callback on Universal link
    func parameters(path: String) -> Parameters?
    
    /// Retrieves the path from the Universal link URL
    /// - Parameter url: the url to be parsed
    func path<AppPath>(from url: URL) -> AppPath where AppPath: RawRepresentable, AppPath.RawValue == String
}
