//
//  UniversalLinkManager.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 03/12/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation

/// Implements this protocol to handle universal deep linking navigation events
public protocol UniversalLinkManager {
    /// Handles universal link in
    /// - Parameter path: the path that needs to be handled
    func handleDeepLink(path: URL)
}
