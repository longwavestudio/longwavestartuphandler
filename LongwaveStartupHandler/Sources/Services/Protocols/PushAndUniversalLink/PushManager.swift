//
//  PushManager.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 03/12/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation

/// Implements this protocol to handle incoming push notification
public protocol PushManager {
    /// The entry point of the push notification. Call this method to handle either remote or local notifications when received.
    /// - Parameters:
    ///   - remote: true if it is a remote notification
    ///   - pushInfo: the payload of the push notification
    func handlePush(remote: Bool, pushInfo: [AnyHashable: Any]) -> Bool
}

