//
//  PathResolver.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 03/12/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation

/// Usually implemented by the Main flow of the app. This is the router delegated to automatically navigate the user in the app
public protocol PathResolver {
    /// Called when the PushManager or the DeepLinkManager needs to ask to navigate somewhere in the app
    /// - Parameter path: the path we need to show to the user
    func navigate(to path: String)
    /// Called when the app needs to reset the current app navigation going back to the main controller
    func resetNavigation()
}
