//
//  PushResolver.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 03/12/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation

protocol PushResolver {
    /// Called every time a push notification needs to be parsed
    /// - Parameter pushIdentifier: the identifier of the push contained inside the payload
    func pushType<PushType>(for pushIdentifier: String) -> PushType where PushType: RawRepresentable, PushType.RawValue == String
    
    /// Called every time the handler needs to know wher redirect the app flow when needed
    /// - Parameter pushType: the push type that triggers the navigation
    func path<PushType, AppPath>(for pushType: PushType) -> AppPath where PushType: RawRepresentable, PushType.RawValue == String, AppPath: RawRepresentable, AppPath.RawValue == String
}

