//
//  StartupService.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 21/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation
import LongwaveUtils

/// Implements to retrieve data needed at startup
public protocol StartupService {
    /// load a confifuration file if needed
    /// - Parameter completion: the completionBlock called when the configuration is ready
    func loadConfiguration(completion: ((_ result: [AnyHashable: Any]?, _ error: Error?) -> Void)?)
}
