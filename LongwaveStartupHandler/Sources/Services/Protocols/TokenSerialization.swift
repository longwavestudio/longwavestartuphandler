//
//  TokenSerialization.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 03/12/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation
import LongwaveAuthenticationHandler

/// Implements this protocol to specify how to handle token storage
public protocol TokenSerialization {
    /// This method will be called when the token is ready to be used and hte user is authenticated
    /// - Parameter token: the token to be used with the authenticated APIs in the app
    func serializeToken(_ token: Token)
}
