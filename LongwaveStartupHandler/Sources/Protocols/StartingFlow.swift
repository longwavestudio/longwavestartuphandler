//
//  StartingFlow.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 19/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import UIKit

public protocol StartingFlow {
    func present(on presenter: UIViewController,
                 onCompletion: @escaping () -> Void)
}
