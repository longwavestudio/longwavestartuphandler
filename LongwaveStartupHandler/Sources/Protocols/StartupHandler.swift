//
//  StartupHandler.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 13/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
import LongwaveNetworkUtils

public protocol StartupHandler: AnyObject {
    var onApiRequestProviderNeeded: (() -> HTTPRequestProvider)? { get set }
    var onApiEngineReady: ((_ apiEngine: ServiceApiEngine, _ configuration: Data?, _ readyForClient: @escaping (Bool) -> Void) -> Void)? { get set }
    var onMainFlowRequested: (() -> StartingFlow)? { get set }
    var onSecureSerializer: (() -> SecureSerializer)? { get set }

    func start<T: LoginResponse>(responceType: T.Type?,
                                 on rootViewController: UIViewController)
    @available(iOS 13.0, *)
    func start<T: LoginResponse>(responceType: T.Type?,
                                 startMode: StartMode,
                                 on rootViewController: UIViewController)
}
