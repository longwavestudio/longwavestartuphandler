//
//  StartupFlowsFactory.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 13/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit

public protocol LoadingViewControllerAnimatable {
    func startLoading()
    func endLoading()
}


public typealias MainFlowViewController = UIViewController & PathResolver
public typealias OnReloadBlock = () -> Void
public typealias LoadingFlowDisplayable = LoadingViewControllerAnimatable & UIViewController

/// Implements this protocol to customize the viewController of the app
public protocol StartupFlowsFactory {
    /// Returns the view controller shown while loading the configuration
    func loadingFlow() -> LoadingFlowDisplayable
    /// Returns the viewController to be shown when an error occurred while loading the configuration
    func startupErrorViewController(onReload: OnReloadBlock?) -> UIViewController
    /// Shows a generic error with the specified title and message
    /// - Parameters:
    ///   - title: the title to be shown
    ///   - message: the message to be shown
    func showGenericError(title: String, message: String) -> UIViewController
    /// Returns the main flow of the app to be loaded after the configuration
    func mainFlow() -> MainFlowViewController?
}

extension StartupFlowsFactory {
    
    public func showGenericError(title: String, message: String) -> UIViewController {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "ok".localizedString(), style: .default, handler: nil))
        return alert
    }
    
    public func startupErrorViewController(onReload: OnReloadBlock?) -> UIViewController {
        let alert = UIAlertController(title: "Errore di configurazione", message: "Errore nello scaricamento del file di configurazione. Assicurati di essere connesso a internet.", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ricarica".localizedString(), style: .default, handler: { _ in
            onReload?()
        }))
        return alert
    }
}
