//
//  AuthenticationHandler+AuthenticationFlow.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation
import LongwaveAuthenticationHandler

extension AuthenticationHandler: AuthenticationFlow {}
