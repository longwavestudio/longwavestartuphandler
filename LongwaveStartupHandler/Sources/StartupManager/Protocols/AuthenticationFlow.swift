//
//  AuthenticationFlow.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 27/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
import LongwaveAuthenticationHandler
import LongwaveNetworkUtils

public protocol AuthenticationFlow {
    
    var onCustomHeaderRequested: OnCustomHeaderRequested? { get set }
    var onThemeRequested: OnThemeRequested { get set }
    
    func setup(service: AuthenticationProvider)
    
    func authenticate<T: LoginResponse>(on presenter: UIViewController,
                      completion: @escaping  (Result<T?, Error>) -> Void)
    
    func refresh<T: LoginResponse>(completion: @escaping (Result<T, Error>) -> Void)
}
