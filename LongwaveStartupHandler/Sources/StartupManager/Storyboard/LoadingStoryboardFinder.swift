//
//  LoadingStoryboardFinder.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 21/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import Foundation
import LongwaveUtils

enum LoadingStoryboardViewControllerId: String {
    case loading = "DefaultLoadingFlowDisplayableViewController"
}

public final class LoadingStoryboardFinder: StoryboardFinder {

    public let storyboardName: String = "Loading"
    
    public init() {}
    
    public func viewControllers(inStoryboard id: String) -> [String] {
        [LoadingStoryboardViewControllerId.loading.rawValue]
    }
}
