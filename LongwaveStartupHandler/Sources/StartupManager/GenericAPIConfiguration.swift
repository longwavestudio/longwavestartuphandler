//
//  GenericAPIConfiguration.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 20/04/2020.
//  Copyright © 2020 LongWave. All rights reserved.
//

import Foundation
import LongwaveNetworkUtils
struct GenericAPIConfiguration: APIConfiguration {
    var baseURL: URL
    init(baseURL: URL) {
        self.baseURL = baseURL
    }
}
