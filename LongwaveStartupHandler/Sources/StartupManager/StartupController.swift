//
//  StartupController.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 13/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
import LongwaveUtils
import LongwaveAuthenticationHandler
import LongwaveNetworkUtils

public struct StartupConfigurator {
    var authenticationMandatory: Bool = false
    
    public init(authenticationMandatory: Bool = false) {
        self.authenticationMandatory = authenticationMandatory
    }
}

public enum StartMode {
    case combine(apis: [APIEndpoint])
    case normal
}

public struct StartupDependencyContainer {
    private(set) var flowFactory: StartupFlowsFactory
    private(set) var startupService: StartupService
    private(set) var startupConfigurator: StartupConfigurator
    private(set) var authenticationHandler: AuthenticationFlow?

    public init(flowFactory: StartupFlowsFactory,
                startupService: StartupService,
                startupConfigurator: StartupConfigurator,
                authenticationHandler: AuthenticationFlow?) {
        self.flowFactory = flowFactory
        self.startupService = startupService
        self.startupConfigurator = startupConfigurator
        self.authenticationHandler = authenticationHandler
    }
}

public final class StartupController {
    var dependencyContainer: StartupDependencyContainer
    var loadingController: UIViewController?
    
    public var onApiRequestProviderNeeded: (() -> HTTPRequestProvider)?
    public var onApiEngineReady: ((_ apiEngine: ServiceApiEngine, _ configuration: Data?, _ readyForClient: @escaping (Bool) -> Void) -> Void)?
    public var onMainFlowRequested: (() -> StartingFlow)?
    public var onSecureSerializer: (() -> SecureSerializer)?
    private var apiEngine: ServiceApiEngine?
    private var mainFlow: StartingFlow?

    private var startMode: StartMode = .normal
    
    public init(dependencyContainer: StartupDependencyContainer) {
        self.dependencyContainer = dependencyContainer
    }
}

extension StartupController: StartupHandler {
    public func start<T: LoginResponse>(responceType: T.Type?, on rootViewController: UIViewController) {
        startApp(responceType: T.self,
                 startMode: .normal,
                 on: rootViewController)
    }
    
    @available(iOS 13.0, *)
    public func start<T: LoginResponse>(responceType: T.Type?,
                                 startMode: StartMode,
                                 on rootViewController: UIViewController) {
        startApp(responceType: T.self,
                 startMode: startMode,
                 on: rootViewController)
    }
    
    private func startApp<T: LoginResponse>(responceType: T.Type?,
                                            startMode: StartMode,
                                            on rootViewController: UIViewController) {
        let loadingFlow = dependencyContainer.flowFactory.loadingFlow()
        loadingController = rootViewController.embed(viewController: loadingFlow)
        loadingFlow.startLoading()
        self.startMode = startMode
        loadConfiguration(responseType: responceType,
                          loadingPresenter: loadingFlow,
                          mainPresenter: rootViewController)
    }
    
    private func loadConfiguration<T: LoginResponse>(responseType: T.Type?,
                                                     loadingPresenter: LoadingFlowDisplayable,
                                                     mainPresenter: UIViewController) {
        dependencyContainer.startupService.loadConfiguration { [weak self, weak loadingPresenter, weak mainPresenter] (configuration, error) in
            guard let self = self else { return }
            guard error == nil else {
                self.showError(responceType: responseType, on: loadingPresenter, mainPresenter: mainPresenter)
                return
            }
            self.createApiEngine(from: configuration, onReadyForClient: { [weak self] ready in
                self?.removeLoadingController()
                guard ready else {
                    self?.showError(responceType: responseType, on: loadingPresenter, mainPresenter: mainPresenter)
                    return
                }
                guard let strongSelf = self,
                    !strongSelf.dependencyContainer.startupConfigurator.authenticationMandatory else {
                    self?.handleMandatoryAuthentication(responceType: responseType, loadingPresenter: loadingPresenter, mainPresenter: mainPresenter)
                    return
                }
                strongSelf.loadMainFlow(presenter: mainPresenter)
            })
        }
    }
    
    private func createApiEngine(from configuration: [AnyHashable: Any]?, onReadyForClient: @escaping (Bool) -> Void) {
        guard let apiRequestor = onApiRequestProviderNeeded?(),
              let baseUrl = configuration?["baseUrl"] as? URL else {
            return
        }
        let queueName = "com.longwave.apimanager"
        let engine: HttpAPIManager
        let configurationApi = GenericAPIConfiguration(baseURL: baseUrl)
        switch startMode {
        case .combine(let apis):
            if #available(iOS 13.0, *) {
                engine = HttpAPIManager(queue: DispatchQueue(label: queueName),
                                        apis: apis,
                                        requestProvider: apiRequestor,
                                        configuration: configurationApi)
            } else {
                engine = HttpAPIManager(queue: DispatchQueue(label: queueName),
                                        requestProvider: apiRequestor,
                                        configuration: configurationApi)
            }
        case .normal:
            engine = HttpAPIManager(queue: DispatchQueue(label: queueName),
                                    requestProvider: apiRequestor,
                                    configuration: configurationApi)
        }
            
        self.apiEngine = engine
        setAuthenticationHandlerService(engine: engine)
        onApiEngineReady?(engine,
                          configuration?["configuration"] as? Data,
                          onReadyForClient)
    }
    
    private func setAuthenticationHandlerService(engine: ServiceApiEngine) {
        guard let serializer = onSecureSerializer?() else { return }
        let authentication = Authentication(withSerializer: serializer)
        let loginService = LoginService(engine: engine,
                                        serializer: serializer,
                                        authentication: authentication)
        dependencyContainer.authenticationHandler?.setup(service: loginService)
    }
    
    private func handleMandatoryAuthentication<T: LoginResponse>(responceType: T.Type?, loadingPresenter: UIViewController?, mainPresenter: UIViewController?) {
        self.loadAuthenticationFlow(responceType: responceType, loadingPresenter: loadingPresenter, mainPresenter: mainPresenter)
    }
    
    private func showError<T: LoginResponse>(responceType: T.Type?, on loadingPresenter: LoadingFlowDisplayable?, mainPresenter: UIViewController?) {
        let factory = self.dependencyContainer.flowFactory
        let alert = factory.startupErrorViewController(onReload: { [weak self] in
            guard let self = self,
                let presenter = loadingPresenter,
                let mainPresenter = mainPresenter else { return }
            self.loadConfiguration(responseType: responceType, loadingPresenter: presenter,
                                   mainPresenter: mainPresenter)
        })
        loadingPresenter?.present(alert, animated: true, completion: nil)
    }
    
    private func removeLoadingController() {
        if let loadingViewcontroller = self.loadingController {
            loadingViewcontroller.removeEmbeddedViewController(embeddedViewController: loadingViewcontroller)
        }
    }
    
    private func handleAuthenticationResult<T: LoginResponse>(_ result: Result<T, Error>,
                                            loadingPresenter: UIViewController?,
                                            mainPresenter: UIViewController) {
        switch result {
        case .success(_):
            self.loadMainFlow(presenter: mainPresenter)
        case .failure(_):
            let alert = self.dependencyContainer.flowFactory.showGenericError(title: "Error".localizedString(), message: "An error occurred!!".localizedString())
            loadingPresenter?.present(alert, animated: true, completion: nil)
            break
        }
    }
    
    private func loadAuthenticationFlow<T: LoginResponse> (responceType: T.Type?, loadingPresenter: UIViewController?, mainPresenter: UIViewController?) {
        guard let mainPresenter = mainPresenter else { return }
        let block:(Result<T?,Error>) -> Void = { [weak self, weak mainPresenter] result in
            guard let self = self, let presenter = mainPresenter else { return }
            switch result {
            case .success(let login):
                guard let login = login else {
                    return
                }
                self.handleAuthenticationResult(.success(login), loadingPresenter: loadingPresenter, mainPresenter: presenter)
            case .failure(let error):
                self.handleAuthenticationResult(Result<T, Error>.failure(error), loadingPresenter: loadingPresenter, mainPresenter: presenter)
            }
        }
        self.dependencyContainer.authenticationHandler?.authenticate(on: mainPresenter, completion: block)
    }
    
    private func loadMainFlow(presenter: UIViewController?) {
        guard let flow = onMainFlowRequested?(),
            let currentPresenter = presenter else {
                let mainFlow = dependencyContainer.flowFactory.mainFlow()
                if let mainFlow = mainFlow {
                    presenter?.embed(viewController: mainFlow)
                }
                return
        }
        mainFlow = flow
        flow.present(on: currentPresenter) { [weak self] in
            self?.mainFlow = nil
        }
    }
}
