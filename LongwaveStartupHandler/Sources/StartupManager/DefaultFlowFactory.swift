//
//  DefaultFlowFactory.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 13/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit
import LongwaveUtils

public final class DefaultFlowFactory {
    private var loadingViewController: LoadingFlowDisplayable?
    private var mainFlowViewController: MainFlowViewController?
    private var finder: StoryboardFinder
   
    public init(loadingViewController: LoadingFlowDisplayable?,
                mainFlowViewController: MainFlowViewController?,
                finder: StoryboardFinder = LoadingStoryboardFinder()) {
        self.loadingViewController = loadingViewController
        self.finder = finder
        self.mainFlowViewController = mainFlowViewController
    }
}

extension DefaultFlowFactory: StartupFlowsFactory {
    
    public func loadingFlow() -> LoadingFlowDisplayable {
        guard let loadingController = loadingViewController else {
            let vc: DefaultLoadingFlowDisplayableViewController = try! DefaultStoryboardLoader.viewController(with: "DefaultLoadingFlowDisplayableViewController", finder: finder, bundle: Bundle(for: DefaultFlowFactory.self))
            self.loadingViewController = vc
            return vc
        }
        return loadingController
    }
    
    public func mainFlow() -> MainFlowViewController? {
        return mainFlowViewController
    }
}
