//
//  DefaultLoadingFlowDisplayable.swift
//  StartupHandler
//
//  Created by Alessio Bonu on 21/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

import UIKit

final class DefaultLoadingFlowDisplayableViewController: UIViewController {
    @IBOutlet weak var roundedOverlay: UIView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var loadingLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    private func setupUI() {
        roundedOverlay.layer.cornerRadius = 10
        activityIndicator.hidesWhenStopped = true
        activityIndicator.isHidden = true
        loadingLabel.text = "Loading"
    }
}


extension DefaultLoadingFlowDisplayableViewController: LoadingViewControllerAnimatable {
    func startLoading() {
        activityIndicator.startAnimating()
    }
    
    func endLoading() {
        activityIndicator.stopAnimating()
    }
}
