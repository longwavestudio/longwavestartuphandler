//
//  StartupHandler.h
//  StartupHandler
//
//  Created by Alessio Bonu on 13/11/2019.
//  Copyright © 2019 LongWave. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for StartupHandler.
FOUNDATION_EXPORT double StartupHandlerVersionNumber;

//! Project version string for StartupHandler.
FOUNDATION_EXPORT const unsigned char StartupHandlerVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <StartupHandler/PublicHeader.h>


