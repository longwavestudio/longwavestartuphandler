# LongwaveStartupHandler

[![CI Status](https://img.shields.io/travis/alessio.bonu@gmail.com/LongwaveStartupHandler.svg?style=flat)](https://travis-ci.org/alessio.bonu@gmail.com/LongwaveStartupHandler)
[![Version](https://img.shields.io/cocoapods/v/LongwaveStartupHandler.svg?style=flat)](https://cocoapods.org/pods/LongwaveStartupHandler)
[![License](https://img.shields.io/cocoapods/l/LongwaveStartupHandler.svg?style=flat)](https://cocoapods.org/pods/LongwaveStartupHandler)
[![Platform](https://img.shields.io/cocoapods/p/LongwaveStartupHandler.svg?style=flat)](https://cocoapods.org/pods/LongwaveStartupHandler)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

LongwaveStartupHandler is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'LongwaveStartupHandler'
```

## Author

alessio.bonu@gmail.com, alessio.bonu@gmail.com

## License

LongwaveStartupHandler is available under the MIT license. See the LICENSE file for more info.
